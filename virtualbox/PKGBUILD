# Based on the file created for Arch Linux by:
# Ionut Biru <ibiru@archlinux.org>
# Sébastien Luttringer <seblu@aur.archlinux.org>

# Maintainer: Philip Müller <philm@manjaro.org>
# Maintainer: Bernhard Landauer <oberon@manjaro.org>
# Contributor: Helmut Stult <helmut[at]manjaro[dot]org>
# torvic9

# enable/disable module signing
_signing=0

_linuxprefix=linux516-vd
_extramodules=extramodules-linux516-vd

pkgbase=$_linuxprefix-virtualbox-modules
pkgname=("$_linuxprefix-virtualbox-host-modules")
pkgver=6.1.30
_pkgver="${pkgver}_OSE"
pkgrel=1
arch=('x86_64')
url='http://virtualbox.org'
license=('GPL')
depends=("$_linuxprefix")
groups=("$_linuxprefix-extramodules")
makedepends=("virtualbox-host-dkms>=$pkgver"
             'dkms'
             "$_linuxprefix" "$_linuxprefix-headers")
source=()

prepare() {
  _kernver="$(</usr/src/${_linuxprefix}/version)"
  cp /usr/src/vboxhost-${_pkgver}/dkms.conf $srcdir/dkms.conf
}

build() {
  _kernver="$(</usr/src/${_linuxprefix}/version)"
  # build host modules
  fakeroot dkms build -c $srcdir/dkms.conf --dkmstree "$srcdir" -m vboxhost/${_pkgver} -k "${_kernver}"
}

package_linux516-vd-virtualbox-host-modules(){
  pkgdesc='VirtualBox host modules for the vd kernel'
  replaces=("$_linuxprefix-virtualbox-modules")
  conflicts=("$_linuxprefix-virtualbox-modules")
  provides=('VIRTUALBOX-HOST-MODULES')
  install=virtualbox-host-modules.install

  _kernver="$(</usr/src/${_linuxprefix}/version)"

  install -dm755 "$pkgdir/usr/lib/modules/$_extramodules"
  cd "vboxhost/$_pkgver/$_kernver/$CARCH/module"

  if [[ ${_signing} -eq 1 ]] ; then
	zstd -d --rm *.ko.zst
  	echo -e "Signing modules with kernel key...\n"
  	for i in vbox{drv,netflt,netadp}.ko; do
  		sign-file516.sh "$i"
  	done
	find ./ -name '*.ko' -exec zstd --rm -10 {} +
  fi

  install -m644 * "$pkgdir/usr/lib/modules/$_extramodules"
  sed -i -e "s/EXTRAMODULES='.*'/EXTRAMODULES='$_extramodules'/" "$startdir/virtualbox-host-modules.install"

  # systemd module loading
  printf '%s\n' vboxdrv vboxnetadp vboxnetflt |
    install -D -m0644 /dev/stdin "$pkgdir/usr/lib/modules-load.d/$pkgname.conf"
}
