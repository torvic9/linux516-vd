## vd kernel 5.16

- better support for clang
- support for module signing
- ProjectC (A. Chen) once released
- lrng (S. Müller)
- block device LED trigger
- better Asus sensors support (nct6775)
- fixes, optimisations and backports

See PKGBUILD for source and more details.
