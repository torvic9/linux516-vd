# Based on the file created for Arch Linux by:
# Tobias Powalowski <tpowa@archlinux.org>
# Thomas Baechler <thomas@archlinux.org>

# Maintainer (vd): torvic9

pkgbase=linux516-vd
pkgname=('linux516-vd' 'linux516-vd-headers')
_basekernel=5.16
_kernelname=-vd
_sub=16
#_rc=rc8
pkgver=${_basekernel}.${_sub}
pkgrel=1
#_stablequeue=fcb8fb1070
arch=('x86_64')
url="http://www.kernel.org/"
license=('GPL2')
makedepends=('xmlto' 'docbook-xsl' 'kmod' 'inetutils' 'bc'
            'python' 'elfutils' 'git' 'libelf' 'pahole')
options=('!strip' '!ccache')
source=(git+https://gitlab.com/torvic9/linux-stable.git#tag=v${pkgver}${_kernelname}
    #git+https://gitlab.com/torvic9/linux-stable.git#branch=torvic-5.16
    #
    # the main kernel config files
    'config.x86_64' 'config.x270' 'config.zen2' 'x509.genkey' "$pkgbase.preset"
    # patches
    'cpupower.patch'
    #
)

validpgpkeys=(
  'ABAF11C65A2970B130ABE3C479BE3E4300411886'  # Linus Torvalds
  '647F28654894E3BD457199BE38DBBDC86092693E'  # Greg Kroah-Hartman
)

sha256sums=('SKIP'
            'a1e67bb874a314874fbad9c4bc59e29e473136e5dc0f8317631b2677c6d63c53'
            '0518975f6fc336cb147871bdb5abb9663bd536554fd1fcc5df31fbf92052b143'
            '6619d6444f0425339a516fb16eb7c9631b64474dfb5207f926fdae8108b0aff2'
            'ab010dc5ef6ce85d352956e5996d242246ecd0912b30f0b72025c38eadff8cd5'
            'c1c97a608ebcdf0f5c6a743880262661e9ae34ce3c2fa8f0995db50e747cf204'
            '962f2f269af9bd75ac3a3579411f1caa4bcfd082ce4abb29ebdb09a99b37d155')

export KBUILD_BUILD_USER=$pkgbase
export KBUILD_BUILD_HOST=arch
export KBUILD_BUILD_TIMESTAMP="$(date -Ru${SOURCE_DATE_EPOCH:+d @$SOURCE_DATE_EPOCH})"

# signing
_signing=0
# edit the paths below to point to your signing keys
_key="$HOME/build/keys/vd516-kernel-key.pem"
_pubkey="$HOME/build/keys/vd516-kernel-pubkey.pem"

# custom clang path
# export PATH=/opt/clang14/bin:$PATH
_clang=0

# export PATH=/opt/gcc11/bin:$PATH

if [[ ${_clang} -eq 1 ]]; then
	LLVMOPTS="LLVM=1"
	CLANGOPTS="CC=clang LD=ld.lld"
else
	LLVMOPTS=""
	CLANGOPTS=""
fi

# Schedulers: cfs,bmq,pds
_sched="cfs"

TB=$(tput bold)
TN=$(tput sgr0)

prepare() {

  cd "${srcdir}/linux-stable"

  ./scripts/setlocalversion --save-scmversion
  echo "-${_kernelname/-/}" > localversion.10-pkgname
  echo "-${pkgrel}" > localversion.20-pkgrel

  echo -e "\n${TB}* APPLYING PATCHES${TN}"

  # patches below
  patch -Np1 -i ../cpupower.patch
  #

  # kernel config
  echo -e "\n${TB}* KERNEL CONFIGURATION${TN}"
  local _config
  echo "---- Select configuration file:"
  echo "${TB}1)${TN} Default"
  echo "${TB}2)${TN} Zen2"
  echo "${TB}3)${TN} X270"
  while true ; do
  	read -p "Enter number (1-3): " _config
	  case ${_config} in
		1) cat ../config.x86_64 > ./.config && break ;;
		2) cat ../config.zen2 > ./.config && break ;;
		3) cat ../config.x270 > ./.config && break ;;
		*) echo "Please enter a number (1-3)!" && continue ;;
  	  esac
  done

  if [[ ${_signing} -eq 1 ]] ; then
  	cat ${_key} > ./certs/vd516-kernel-key.pem
  	cat ${_pubkey} > ./certs/vd516-kernel-pubkey.pem
  	sed -i "s|signing_key|vd516-kernel-key|" ./.config
  	sed -ri "s|^(CONFIG_SYSTEM_TRUSTED_KEYS=).*|\1\"certs/vd516-kernel-pubkey.pem\"|" ./.config
  else
  	cat ../x509.genkey > ./certs/x509.genkey
  fi

  sed -i '2iexit 0' ./scripts/depmod.sh
  #make $LLVMOPTS prepare
  make $LLVMOPTS oldconfig

  if [[ ${_sched} == "bmq" || ${_sched} == "pds" ]]; then
  	./scripts/config -e CONFIG_SCHED_ALT
  	[[ ${_config} -eq 2 ]] && ./scripts/config -d CONFIG_CPU_FREQ_DEFAULT_GOV_SCHEDUTIL && \
  	  ./scripts/config -e CONFIG_CPU_FREQ_DEFAULT_GOV_ONDEMAND
  	[[ ${_sched} == "bmq" ]] && ./scripts/config -e CONFIG_SCHED_BMQ
  	[[ ${_sched} == "pds" ]] && ./scripts/config -e CONFIG_SCHED_PDS
  fi

  # get kernel version
  make $LLVMOPTS -s kernelrelease > version
  printf "\n  Prepared %s version %s\n" "$pkgbase" "$(<version)"
  read -p "---- Enter 'y' for nconfig: " NCONFIG
  [[ $NCONFIG == "y" ]] && make $LLVMOPTS nconfig

  # rewrite configuration
  yes '' | make $LLVMOPTS config >/dev/null
}

build() {
  cd "${srcdir}/linux-stable"

  # build!
  make $LLVMOPTS LOCALVERSION= bzImage modules

  # build cpupower
  make $LLVMOPTS -C tools/power/cpupower
}

package_linux516-vd() {
  pkgdesc="The ${pkgbase/linux/Linux} vd kernel and modules"
  depends=('coreutils' 'linux-firmware' 'kmod' 'mkinitcpio>=27')
  optdepends=('crda: to set the correct wireless channels of your country')
  provides=(VIRTUALBOX-GUEST-MODULES)

  cd "${srcdir}/linux-stable"

  local kernver="$(<version)"
  local modulesdir="$pkgdir/usr/lib/modules/$kernver"

  # systemd expects to find the kernel here to allow hibernation
  # https://github.com/systemd/systemd/commit/edda44605f06a41fb86b7ab8128dcf99161d2344
  install -Dm644 "$(make $LLVMOPTS -s image_name)" "$modulesdir/vmlinuz"

  # Used by mkinitcpio to name the kernel
  echo "${pkgbase}" | install -Dm644 /dev/stdin "$modulesdir/pkgbase"

  # make room for external modules
  local _extramodules="extramodules-${pkgbase}"
  ln -s "../${_extramodules}" "$modulesdir/extramodules"
  # add real version for building modules and running depmod from hook
  echo "${kernver}" |
    install -Dm644 /dev/stdin "${pkgdir}/usr/lib/modules/${_extramodules}/version"

  echo -e "\n${TB}* INSTALLING MODULES${TN}"
  make $LLVMOPTS LOCALVERSION= INSTALL_MOD_PATH="${pkgdir}/usr" INSTALL_MOD_STRIP=1 modules_install

  # remove build and source links
  rm "${modulesdir}/source"
  rm "${modulesdir}/build"
  [[ -f ./certs/vd516-kernel-key.pem ]] && rm ./certs/vd516-kernel-key.pem

  # add mkinitcpio preset (not strictly needed)
  install -Dm644 "$srcdir/${pkgbase}.preset" "$pkgdir/etc/mkinitcpio.d/${pkgbase}.preset"

  # depmod
  depmod -b "${pkgdir}/usr" -F System.map "${kernver}"
}

package_linux516-vd-headers() {
  pkgdesc="Header files and scripts for building modules for ${pkgbase/linux/Linux} vd kernel"

  cd "${srcdir}/linux-stable"
  local kernver="$(<version)"
  local _builddir="${pkgdir}/usr/lib/modules/${kernver}/build"

  echo -e "\n${TB}* INSTALLING HEADERS${TN}"
  install -Dt "${_builddir}" -m644 Makefile .config Module.symvers System.map version vmlinux localversion.*
  install -Dt "${_builddir}/kernel" -m644 kernel/Makefile
  install -Dt "${_builddir}/arch/x86" -m644 "arch/x86/Makefile"
  #mkdir "${_builddir}/.tmp_versions"

  cp -t "${_builddir}" -a include scripts

  # add objtool for external module building and enabled VALIDATION_STACK option
  install -Dt "${_builddir}/tools/objtool" tools/objtool/objtool

  # add cpupower
  install -Dt "${_builddir}/tools/cpupower" tools/power/cpupower/cpupower
  install -Dt "${_builddir}/tools/cpupower" tools/power/cpupower/libcpupower.so.0.0.1

  # add xfs and shmem for aufs building
  mkdir -p "${_builddir}"/{fs/xfs,mm}

  cp -t "${_builddir}/arch/x86" -a "arch/x86/include"
  install -Dt "${_builddir}/arch/x86/kernel" -m644 "arch/x86/kernel/asm-offsets.s"

  install -Dt "${_builddir}/drivers/md" -m644 drivers/md/*.h
  install -Dt "${_builddir}/net/mac80211" -m644 net/mac80211/*.h

  # http://bugs.archlinux.org/task/13146
  install -Dt "${_builddir}/drivers/media/i2c" -m644 drivers/media/i2c/msp3400-driver.h

  # http://bugs.archlinux.org/task/20402
  install -Dt "${_builddir}/drivers/media/usb/dvb-usb" -m644 drivers/media/usb/dvb-usb/*.h
  install -Dt "${_builddir}/drivers/media/dvb-frontends" -m644 drivers/media/dvb-frontends/*.h
  install -Dt "${_builddir}/drivers/media/tuners" -m644 drivers/media/tuners/*.h

  # copy in Kconfig files
  find . -name Kconfig\* -exec install -Dm644 {} "${_builddir}/{}" \;

  # remove unneeded stuff
  echo -e "\n${TB}* REMOVING UNNEEDED FILES${TN}"
  # remove unneeded architectures
  local _arch
  for _arch in "${_builddir}"/arch/*/; do
    [[ ${_arch} == */x86/ ]] && continue
    rm -r "${_arch}"
  done

  # remove files already in linux-docs package
  rm -r "${_builddir}/Documentation"

  # remove broken symlinks
  find -L "${_builddir}" -type l -printf 'Removing %P\n' -delete

  # remove loose objects"
  find "${_builddir}" -type f -name '*.o' -printf 'Removing %P\n' -delete

  # strip scripts directory
  echo -e "\n${TB}* STRIPPING${TN}"
  local file
  while read -rd '' file; do
    case "$(file -bi "$file")" in
      application/x-sharedlib\;*)      # Libraries (.so)
        strip -v $STRIP_SHARED "$file" ;;
      application/x-archive\;*)        # Libraries (.a)
        strip -v $STRIP_STATIC "$file" ;;
      application/x-executable\;*)     # Binaries
        strip -v $STRIP_BINARIES "$file" ;;
      application/x-pie-executable\;*) # Relocatable binaries
        strip -v $STRIP_SHARED "$file" ;;
    esac
  done < <(find "${_builddir}" -type f -perm -u+x ! -name vmlinux -print0)

  strip -v $STRIP_STATIC "${_builddir}/vmlinux"

  echo -e "\n${TB}* SYMLINKING${TN}"
  mkdir -p "$pkgdir/usr/src"
  ln -sr "$_builddir" "$pkgdir/usr/src/$pkgbase"
}
